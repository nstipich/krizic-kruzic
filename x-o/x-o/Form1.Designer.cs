﻿namespace x_o
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.X0 = new System.Windows.Forms.Button();
            this.X1 = new System.Windows.Forms.Button();
            this.X2 = new System.Windows.Forms.Button();
            this.Y0 = new System.Windows.Forms.Button();
            this.Y1 = new System.Windows.Forms.Button();
            this.Y2 = new System.Windows.Forms.Button();
            this.Z0 = new System.Windows.Forms.Button();
            this.Z1 = new System.Windows.Forms.Button();
            this.Z2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.player1lbl = new System.Windows.Forms.Label();
            this.xlbl = new System.Windows.Forms.Label();
            this.player2lbl = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oIgriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novaIgraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izbrisiPobjedeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izlazToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // X0
            // 
            this.X0.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X0.Location = new System.Drawing.Point(9, 58);
            this.X0.Name = "X0";
            this.X0.Size = new System.Drawing.Size(120, 120);
            this.X0.TabIndex = 0;
            this.X0.UseVisualStyleBackColor = true;
            this.X0.Click += new System.EventHandler(this.button_click);
            // 
            // X1
            // 
            this.X1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X1.Location = new System.Drawing.Point(163, 58);
            this.X1.Name = "X1";
            this.X1.Size = new System.Drawing.Size(120, 120);
            this.X1.TabIndex = 1;
            this.X1.UseVisualStyleBackColor = true;
            this.X1.Click += new System.EventHandler(this.button_click);
            // 
            // X2
            // 
            this.X2.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X2.Location = new System.Drawing.Point(323, 58);
            this.X2.Name = "X2";
            this.X2.Size = new System.Drawing.Size(120, 120);
            this.X2.TabIndex = 2;
            this.X2.UseVisualStyleBackColor = true;
            this.X2.Click += new System.EventHandler(this.button_click);
            // 
            // Y0
            // 
            this.Y0.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Y0.Location = new System.Drawing.Point(9, 199);
            this.Y0.Name = "Y0";
            this.Y0.Size = new System.Drawing.Size(120, 120);
            this.Y0.TabIndex = 3;
            this.Y0.UseVisualStyleBackColor = true;
            this.Y0.Click += new System.EventHandler(this.button_click);
            // 
            // Y1
            // 
            this.Y1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Y1.Location = new System.Drawing.Point(163, 199);
            this.Y1.Name = "Y1";
            this.Y1.Size = new System.Drawing.Size(120, 120);
            this.Y1.TabIndex = 4;
            this.Y1.UseVisualStyleBackColor = true;
            this.Y1.Click += new System.EventHandler(this.button_click);
            // 
            // Y2
            // 
            this.Y2.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Y2.Location = new System.Drawing.Point(323, 199);
            this.Y2.Name = "Y2";
            this.Y2.Size = new System.Drawing.Size(120, 120);
            this.Y2.TabIndex = 5;
            this.Y2.UseVisualStyleBackColor = true;
            this.Y2.Click += new System.EventHandler(this.button_click);
            // 
            // Z0
            // 
            this.Z0.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Z0.Location = new System.Drawing.Point(9, 334);
            this.Z0.Name = "Z0";
            this.Z0.Size = new System.Drawing.Size(120, 120);
            this.Z0.TabIndex = 6;
            this.Z0.UseVisualStyleBackColor = true;
            this.Z0.Click += new System.EventHandler(this.button_click);
            // 
            // Z1
            // 
            this.Z1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Z1.Location = new System.Drawing.Point(163, 334);
            this.Z1.Name = "Z1";
            this.Z1.Size = new System.Drawing.Size(120, 120);
            this.Z1.TabIndex = 7;
            this.Z1.UseVisualStyleBackColor = true;
            this.Z1.Click += new System.EventHandler(this.button_click);
            // 
            // Z2
            // 
            this.Z2.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Z2.Location = new System.Drawing.Point(323, 334);
            this.Z2.Name = "Z2";
            this.Z2.Size = new System.Drawing.Size(120, 120);
            this.Z2.TabIndex = 8;
            this.Z2.UseVisualStyleBackColor = true;
            this.Z2.Click += new System.EventHandler(this.button_click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 472);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(193, 472);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Nerijeseno:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(367, 472);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "label3";
            // 
            // player1lbl
            // 
            this.player1lbl.AutoSize = true;
            this.player1lbl.Location = new System.Drawing.Point(46, 512);
            this.player1lbl.Name = "player1lbl";
            this.player1lbl.Size = new System.Drawing.Size(13, 13);
            this.player1lbl.TabIndex = 12;
            this.player1lbl.Text = "0";
            // 
            // xlbl
            // 
            this.xlbl.AutoSize = true;
            this.xlbl.Location = new System.Drawing.Point(203, 512);
            this.xlbl.Name = "xlbl";
            this.xlbl.Size = new System.Drawing.Size(13, 13);
            this.xlbl.TabIndex = 13;
            this.xlbl.Text = "0";
            // 
            // player2lbl
            // 
            this.player2lbl.AutoSize = true;
            this.player2lbl.Location = new System.Drawing.Point(367, 512);
            this.player2lbl.Name = "player2lbl";
            this.player2lbl.Size = new System.Drawing.Size(13, 13);
            this.player2lbl.TabIndex = 14;
            this.player2lbl.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(203, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "label7";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.oIgriToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(455, 24);
            this.menuStrip1.TabIndex = 16;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novaIgraToolStripMenuItem,
            this.izbrisiPobjedeToolStripMenuItem,
            this.izlazToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // oIgriToolStripMenuItem
            // 
            this.oIgriToolStripMenuItem.Name = "oIgriToolStripMenuItem";
            this.oIgriToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.oIgriToolStripMenuItem.Text = "O igri";
            this.oIgriToolStripMenuItem.Click += new System.EventHandler(this.oIgriToolStripMenuItem_Click);
            // 
            // novaIgraToolStripMenuItem
            // 
            this.novaIgraToolStripMenuItem.Name = "novaIgraToolStripMenuItem";
            this.novaIgraToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.novaIgraToolStripMenuItem.Text = "Nova Igra";
            this.novaIgraToolStripMenuItem.Click += new System.EventHandler(this.novaIgraToolStripMenuItem_Click);
            // 
            // izbrisiPobjedeToolStripMenuItem
            // 
            this.izbrisiPobjedeToolStripMenuItem.Name = "izbrisiPobjedeToolStripMenuItem";
            this.izbrisiPobjedeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.izbrisiPobjedeToolStripMenuItem.Text = "Izbrisi Pobjede";
            this.izbrisiPobjedeToolStripMenuItem.Click += new System.EventHandler(this.izbrisiPobjedeToolStripMenuItem_Click);
            // 
            // izlazToolStripMenuItem
            // 
            this.izlazToolStripMenuItem.Name = "izlazToolStripMenuItem";
            this.izlazToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.izlazToolStripMenuItem.Text = "Izlaz";
            this.izlazToolStripMenuItem.Click += new System.EventHandler(this.izlazToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 537);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.player2lbl);
            this.Controls.Add(this.xlbl);
            this.Controls.Add(this.player1lbl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Z2);
            this.Controls.Add(this.Z1);
            this.Controls.Add(this.Z0);
            this.Controls.Add(this.Y2);
            this.Controls.Add(this.Y1);
            this.Controls.Add(this.Y0);
            this.Controls.Add(this.X2);
            this.Controls.Add(this.X1);
            this.Controls.Add(this.X0);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button X0;
        private System.Windows.Forms.Button X1;
        private System.Windows.Forms.Button X2;
        private System.Windows.Forms.Button Y0;
        private System.Windows.Forms.Button Y1;
        private System.Windows.Forms.Button Y2;
        private System.Windows.Forms.Button Z0;
        private System.Windows.Forms.Button Z1;
        private System.Windows.Forms.Button Z2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label player1lbl;
        private System.Windows.Forms.Label xlbl;
        private System.Windows.Forms.Label player2lbl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oIgriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novaIgraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izbrisiPobjedeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izlazToolStripMenuItem;
    }
}

