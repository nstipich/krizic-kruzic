﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace x_o
{
    public partial class Form1 : Form
    {
        bool X = true;
        int counter = 0;
        static String player1, player2;


        public Form1()
        {
            InitializeComponent();
        }

        public static void postavi_imena(string n1, string n2)
        {
            player1 = n1;
            player2 = n2;
        }

        private void button_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (X)
            {
                b.Text = "X";
                label7.Text = player2 + " je na potezu!";

            }
            else
            {
                b.Text = "O";
                label7.Text = player1 + " je na potezu!";

            }
            X = !X;
            b.Enabled = false;
            counter++;
            provjeri_pobjednika();
        }

        private void provjeri_pobjednika()
        {
            bool winner = false;
            //vodoravna provjera
            if ((X0.Text == X1.Text) && (X1.Text == X2.Text) && (!X0.Enabled))
            { winner = true; }
            else if ((Y0.Text == Y1.Text) && (Y1.Text == Y2.Text) && (!Y0.Enabled))
            { winner = true; }
            else if ((Z0.Text == Z1.Text) && (Z1.Text == Z2.Text) && (!Z0.Enabled))
            { winner = true; }
            //okomita provjera
            else if ((X0.Text == Y0.Text) && (Y0.Text == Z0.Text) && (!X0.Enabled))
            { winner = true; }
            else if ((X1.Text == Y1.Text) && (Y1.Text == Z1.Text) && (!X1.Enabled))
            { winner = true; }
            else if ((X2.Text == Y2.Text) && (Y2.Text == Z2.Text) && (!X2.Enabled))
            { winner = true; }
            //dijagonalna provjera
            else if ((X0.Text == Y1.Text) && (Y1.Text == Z2.Text) && (!X0.Enabled))
            { winner = true; }
            else if ((X2.Text == Y1.Text) && (Y1.Text == Z0.Text) && (!X2.Enabled))
            { winner = true; }


            if (winner)
            {
                disable_buttons();
                String pobjednik = "";
                if (X)
                {
                    pobjednik = player2;
                    player2lbl.Text = (Int32.Parse(player2lbl.Text) + 1).ToString();

                }
                else
                {
                    pobjednik = player1;
                    player1lbl.Text = (Int32.Parse(player1lbl.Text) + 1).ToString();
                }
                MessageBox.Show(pobjednik + " je pobjedio!", "Rezultat");
            }
            else
            {
                if (counter == 9)
                {
                    xlbl.Text = (Int32.Parse(xlbl.Text) + 1).ToString();
                    MessageBox.Show("Izjednaceno!", "Rezultat");
                }
            }

        } // end provjeri_pobjedinka

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void novaIgraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            X = true;
            counter = 0;

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }

                catch { }
            }
        }

        private void izbrisiPobjedeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            player1lbl.Text = "0";
            xlbl.Text = "0";
            player2lbl.Text = "0";

            X = true;
            counter = 0;

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }

                catch { }
            }
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.ShowDialog();
            label7.Text = player1 + " je na potezu!";
            label1.Text = player1;
            label3.Text = player2;
            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.BackColor = Color.Green;
                }

                catch { }
            }


        }

        private void oIgriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Igra Križić Kružić za 2 igrača.", "Hello");
        }

        private void disable_buttons()
        {

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
                catch { }
            }


        }

    }
}
